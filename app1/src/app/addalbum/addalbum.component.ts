import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addalbum',
  templateUrl: './addalbum.component.html',
  styleUrls: ['./addalbum.component.css']
})
export class AddalbumComponent implements OnInit {


  albumId;
  albumName=''
  releaseDate
  numOfTracks;
  image
  song


  constructor(private user :UserService,private router:Router) { }

  ngOnInit() {
  }

  onSelectImage(event) {
    console.log(event);
    this.image = event.target.files[0];
    console.log((this.image))
}
onSelectSong(event) {
  console.log(event);
  this.song=(event.target.files[0]) ;
  console.log((this.song))
}


onAdd() {
  console.log(this.image)
  this.user
    .addalbum(this.albumId, this.albumName, this.releaseDate, this.numOfTracks,
          this.image,this.song)
    .subscribe(response => {
      console.log(response);
      if (response['_body']  == 'album added') {
      this.router.navigate(["/albumlist"]);
      }
      else{
        alert("album not added...");
      }
    });
}

}
