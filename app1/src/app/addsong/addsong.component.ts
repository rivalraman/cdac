import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-addsong',
  templateUrl: './addsong.component.html',
  styleUrls: ['./addsong.component.css']
})
export class AddsongComponent implements OnInit {

  aid;
  songId;
  songName;
  songlength;
  song;
  


  constructor(private userservice:UserService,private activeroute:ActivatedRoute,private router:Router) {

    activeroute.queryParams.subscribe(param=>{
      this.aid=param['id'];
    })
}


onSelectImage(event) {
    console.log(event);
    this.song =   event.target.files[0];
   // this.song=btoa(this.song)
    
}

onAdd() {
  
console.log("ts mein "+this.song)

  this.userservice
    .addsong(this.song)
    .subscribe(response => {
      console.log(response);
      if (response['_body']  == 'song added') {
      this.router.navigate(["/singlealbum"]);
      }
      else{
        alert("Song not added...");
      }
    });
}

onCancel()
{
  this.router.navigate(["/singlealbum"]);
}






  ngOnInit() {
  }

}
