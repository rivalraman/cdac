import { Component, OnInit } from '@angular/core';
import { UserlistComponent } from '../userlist/userlist.component';

@Component({
  selector: 'app-adminpage',
  templateUrl: './adminpage.component.html',
  styleUrls: ['./adminpage.component.css']
})
export class AdminpageComponent implements OnInit {

  constructor(private U:UserlistComponent) { this.U.getuserlist()}

  ngOnInit() {
  }

}
