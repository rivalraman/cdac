import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-albumlist',
  templateUrl: './albumlist.component.html',
  styleUrls: ['./albumlist.component.css']
})
export class AlbumlistComponent implements OnInit {

albumlist=[];
songs=[];

  constructor(private userser:UserService,private router:Router) {this.showalbum() }

  ngOnInit() {
  }

editalbum(id :number)
{

  this.router.navigate(['/singlealbum'],{queryParams:{id:id}});


console.log(id)
}

showalbum()
{
  this.userser.showalbum().subscribe(resp=>{
    console.log(resp.json());
this.albumlist=resp.json();
  })
}

}
