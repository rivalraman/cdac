import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { F1Component } from './f1/f1.component';
import { F2Component } from './f2/f2.component';
import { F3Component } from './f3/f3.component';
import { LoginComponent } from './login/login.component';
import { PlayerComponent } from './player/player.component';
import { UserService } from './user.service';
import { HttpModule } from '@angular/http';
import { RegisterComponent } from './register/register.component';
import { AdminpageComponent } from './adminpage/adminpage.component';
import { UserlistComponent } from './userlist/userlist.component';
import { AlbumlistComponent } from './albumlist/albumlist.component';
import { AddalbumComponent } from './addalbum/addalbum.component';
import { SinglealbumComponent } from './singlealbum/singlealbum.component';
import { AddsongComponent } from './addsong/addsong.component';
//import { SongsunderalbumComponent } from './songsunderalbum/songsunderalbum.component';


@NgModule({
  declarations: [
    AppComponent,
    F1Component,
    F2Component,
    F3Component,
    LoginComponent,
    PlayerComponent,
    RegisterComponent,
    AdminpageComponent,
    UserlistComponent,
    AlbumlistComponent,
    AddalbumComponent,
    SinglealbumComponent,
    AddsongComponent,
    
  ],
  imports: [
    BrowserModule,FormsModule,HttpModule,
    RouterModule.forRoot([
      //{ path: 'movie-add', component: AddMovieComponent, canActivate: [UserService] },
      { path: 'login', component: LoginComponent },
      { path: 'addalbum', component: AddalbumComponent },
      { path: 'adminpage', component: AdminpageComponent },
      { path: 'register', component: RegisterComponent },
      { path: '', component: F2Component },
      { path: '', component: F3Component },
      { path: 'userlist', component: UserlistComponent },
      { path: 'albumlist', component: AlbumlistComponent },
      { path: 'singlealbum', component: SinglealbumComponent },
      { path: 'addsong', component: AddsongComponent },
     
      
    ])
  ],
  providers: [UserService,UserlistComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
