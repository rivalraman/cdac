import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email = '';
  password = '';
  constructor( private userService: UserService,
    private router: Router) { }

  ngOnInit() {
  }

  onLogin() {
    if (this.email.length == 0) {
      alert('enter email');
    } else if (this.password.length == 0) {
      alert('enter password');
    } else {
      this.userService
        .login(this.email, this.password)
        .subscribe(response => {
          
          if (response['_body'] == 'admin') {

            // cache the login status
            // localStorage
            sessionStorage['login_status'] = '1';
            debugger;
            alert('welcome to the app admin');
            var x = document.getElementById("loginbtn");
            x.setAttribute("style","display:none;");
            var y = document.getElementById("logoutbtn");
            y.setAttribute("style","display:block-inline;margin-left: 20px; margin-top: 7px");
            var z = document.getElementById("signupbtn");
            z.setAttribute("style","display:none;margin:left:10px;");
            this.router.navigate(['/adminpage']);
            this.router.navigate(['/userlist']);
            
          }
          else if(response['_body'] == 'user')
          {
            sessionStorage['login_status'] = '1';

          
            alert('welcome to the app user');
            var x = document.getElementById("loginbtn");
            x.setAttribute("style","display:none;");
            var y = document.getElementById("logoutbtn");
            y.setAttribute("style","display:block-inline;");
            var z = document.getElementById("signupbtn");
            z.setAttribute("style","display:block-inline;");




            this.router.navigate(['/']);
          }
          
          
          else {
            alert(response['_body']);
          }
        });
    }
  }

  oncancel()
  {
    this.router.navigate(['/']);
  }

}
