import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  fname = '';
  lname = '';
  email = '';
  password = '';
  confirmPassword = '';
  role ='user';


  constructor( private router: Router,
    private userService: UserService) { }

  ngOnInit() {
  }

  onSignup() {
    if (this.fname.length == 0) {
      alert('enter first name');
    } else if (this.lname.length == 0) {
      alert('enter last name');
    } else if (this.email.length == 0) {
      alert('enter email');
    } else if (this.password.length == 0) {
      alert('enter password');
    } else if (this.confirmPassword.length == 0) {
      alert('confirm password');
    } else if (this.confirmPassword != this.password) {
      alert('password do not match');
    } else {
      this.userService
        .signup(this.fname, this.lname, this.email, this.password,this.role)
        .subscribe(response => {
          
          if (response['_body'] == 'register_Successfull') {
            alert('Registration successfull');
            this.router.navigate(['/login']);
          } else {
            alert('error while registering a user');
          }
        });
    }
  }
  onCancel() {
    this.router.navigate(['/login']);
  }

}
