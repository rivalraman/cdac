import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglealbumComponent } from './singlealbum.component';

describe('SinglealbumComponent', () => {
  let component: SinglealbumComponent;
  let fixture: ComponentFixture<SinglealbumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinglealbumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglealbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
