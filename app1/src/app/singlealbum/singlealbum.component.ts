import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-singlealbum',
  templateUrl: './singlealbum.component.html',
  styleUrls: ['./singlealbum.component.css']
})
export class SinglealbumComponent implements OnInit {

  id;
  songs=[];
  al;
  constructor(private userservice:UserService,private activeroute:ActivatedRoute,private router:Router) { 

    activeroute.queryParams.subscribe(param=>{
      this.id=param['id'];
 
      this.userservice.albumimage(this.id).subscribe(resp=>{
        this.al=resp.json();
        console.log("image"+this.al["image"])
      })
      this.userservice.showalbumbyid(this.id).subscribe(resp=>{
      this.songs=resp.json();
      })
    })
  }
  ngOnInit() {
  }

addsong()
{
  this.router.navigate(['/addsong'],{queryParams:{id:this.id}})
}


play()
{
var x=document.getElementById("player").setAttribute("src","data:audio/mpeg;base64,"+this.songs[0].song);
sessionStorage.setItem("song",this.songs[0].song)

}



}
