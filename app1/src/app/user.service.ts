import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  //ng saku1="http://192.168.42.191:8080/Project/User"
saku1='http://172.0.2.217:8080/Project/User';
saku2='http://172.0.2.217:8080/Project/Admin';
  url1 = 'http://localhost:8080/Project/User';
  url2= 'http://localhost:8080/Project/Admin';
  constructor(private router: Router,
    private http: Http) { }

    login(email: string, password: string) {
      const body = {
        email: email,
        password: password
      };
    const headers = new Headers({'Content-Type': 'application/json'});
    const requestOptions = new RequestOptions({headers: headers});
    return this.http.post(this.saku1 + '/login', body, requestOptions);
    }


    signup(fname: string, lname: string, email: string, password: string,role:string) {
      const body = {
        fname: fname,
        lname: lname,
        email: email,
        password: password,
        role:role
      };
      const headers = new Headers({'Content-Type': 'application/json'});
      const requestOptions = new RequestOptions({headers: headers});
      return this.http.post(this.saku1 + '/register', body, requestOptions);
    }
    

getuserlist()
{
  return this.http.get(this.saku2+'/userlist')
}

deleteuser(id:Number)
{
  return this.http.delete(this.saku1+'/'+id);
}

editalbum(id:number)
{
  return this.http.get(this.saku2+'/'+id);
}

showalbum()
{
  return this.http.get(this.saku1+'/albumlist')
}
showalbumbyid(id :number)
{
return  this.http.get(this.saku1+'/singlealbum/'+id);

}

albumimage(id:number)
{
return this.http.get(this.saku1+'/imagededo/'+id)
}


addalbum(albumId:number, albumName: string,releaseDate: Date, numOfTracks: number,
  image: any,song:any)
{
  const formData = new FormData();
    formData.append('albumId',''+albumId);
    formData.append('albumName', albumName);
    formData.append('releaseDate', '' + releaseDate);
    formData.append('numOfTracks', ''+numOfTracks);
    formData.append('image', image);
    formData.append('song', song);
   

    console.log("SAnku "+image)
   
    return this.http.post(this.saku2+'/addalbum', formData);
  }

addsong(song:any)
{

  const formData = new FormData();
  // formData.append('songId',''+songId);
  // formData.append('songName', songName);
  // formData.append('songlength', '' + songlength);
  // formData.append('albumId',''+ albumId);
  formData.append('song', ''+  song);
  

  //console.log("service mein "+songId+" "+songName+" "+songlength+" "+albumId+" "+song)

  return this.http.post(this.saku2+'/addsong2', formData);
 
  
}

addsong2(song:any)
{

  const formData = new FormData();
 
  formData.append('song', ''+  song);
  

  //console.log("service mein "+songId+" "+songName+" "+songlength+" "+albumId+" "+song)

  return this.http.post(this.saku2+'/addsong2', formData);
 
  
}



}
































