import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { UserService } from "../user.service";
@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  userlist=[];
  constructor(private userservice:UserService,private router:Router) { this.getuserlist()}

  ngOnInit() {
  }

getuserlist()
{
this.userservice.getuserlist().subscribe(resp=>{
  console.log(resp['_body']);
  console.log(this.userlist=resp.json());
 // this.userlist=resp['_body'];
})
}

deleteuser(id:Number)
{
  console.log(id)
const res=confirm("Sure to delete");
if(res)
  {
      this.userservice.deleteuser(id).subscribe(res=>{
       this.getuserlist();
      })
  }


}

}
